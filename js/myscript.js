function get_buku() {
    var action = 'get_all';
    var no = 0;
    $.ajax({
        url: 'services.php',
        type: 'GET',
        //masukan parameter
        data: {
            action: action,
        },
        success: function (data) {
            //menampilkan data di console log
            console.log(data);
            //memasukan data ke html
            for (x in data.data) {
                ++no;
                $('#isi-tabel').append('<tr id="' + data.data[x].id_buku + '"><td>' + no + '</td>' + '<td><div style="width: 180px"><div class="thumbnail"><img class="img img-responsive" src="img/' + data.data[x].cover + '"></div></div></td>' +
                    '<td data-target="judul">' + data.data[x].judul + '</td>' +
                    '<td>' + data.data[x].pengarang + '</td>' +
                    '<td>' + data.data[x].penerbit + '</td>' +
                    '<td>' + data.data[x].nama_kategori + '</td>' +
                    '<td><a class="btn btn-success" href="ubah_buku.php?id_buku=' + data.data[x].id_buku + '"><i class="fa fa-edit"></i></a>' +
                    '<br><button onclick="delete_data(' + data.data[x].id_buku + ')" class="btn btn-danger"><i class="fa fa-trash"></i></button></td>' +
                    '</tr>');
            }
        },
        error: function (data) {
            console.log(data);
        }
    })
}

function tambah_data() {
    var action = 'insert_buku';
    var cover = $("input[name=filefoto]").val();
    var kategori = $("#kategori option:selected").val();
    var judul = $("input[name=judul]").val();
    var pengarang = $("input[name=pengarang]").val();
    var penerbit = $("input[name=penerbit]").val();
    var isbn = $("input[name=isbn]").val();
    var tahun_terbit = $("input[name=tahunterbit]").val();
    $.ajax({
        url: 'services.php',
        type: 'post',
        //masukan parameter
        data: {
            action: action,
            cover: cover,
            id_kategori: kategori,
            judul: judul,
            pengarang: pengarang,
            penerbit: penerbit,
            isbn: isbn,
            tahun_terbit: tahun_terbit
        },
        success: function (data) {
            //menampilkan data di console log
            console.log(data);
            //memasukan data ke html
            alert('Data berhasil di tambahkan');
            window.location = "index.php";
        },
        error: function (data) {
            console.log(data);
            alert('Data gagal di tambahkan');
            window.location = "tambah_buku.php";
        }
    })
}

function delete_data(id) {
    var action = 'delete_buku';
    var id_buku = id;
    if (confirm('Apakah yakin ingin menghapus ? ')) {
        $.ajax({
            url: 'services.php',
            type: 'GET',
            data: {
                action: action,
                id_buku: id_buku
            },
            success: function (data) {
                console.log(data);
                alert('Data berhasil di hapus');
                document.location.reload(true);
            },
            error: function (data) {
                console.log(data);
                alert('Data gagal dihapus');
                document.location.reload(true);
            }
        })
    }
}

function get_update() {
    var action = "get_data";
    var parts = window.location.search.substr(1).split("&");
    var $_GET = {};

    for (var i = 0; i < parts.length; i++){
        var temp = parts[i].split("=");
        $_GET[decodeURIComponent(temp[0])] = decodeURIComponent(temp[1]);
    }

    var id_buku = $_GET["id_buku"];
    $.ajax({
        url: "services.php",
        type: "GET",

        data: {
          action: action,
          id_buku: id_buku
        },

        success: function (data) {
            console.log(data);

            for (x in data.data){
                $("input[name=id]").val(data.data[x].id_buku);
                $("input[name=filefoto]").val(data.data[x].cover);
                $("#kategori").val(data.data[x].id_kategori);
                $("input[name=judul]").val(data.data[x].judul);
                $("input[name=pengarang]").val(data.data[x].pengarang);
                $("input[name=penerbit]").val(data.data[x].penerbit);
                $("input[name=isbn]").val(data.data[x].isbn);
                $("input[name=tahunterbit]").val(data.data[x].tahun_terbit);
            }
        },

        error: function (data) {
            console.log(data);
        }
    })
}

function update_data() {
    var action = 'update_buku';
    var parts = window.location.search.substr(1).split("&");
    var $_GET = {};

    for (var i = 0; i < parts.length; i++){
        var temp = parts[i].split("=");
        $_GET[decodeURIComponent(temp[0])] = decodeURIComponent(temp[1]);
    }

    var id_buku = $_GET["id_buku"];

    var cover = $("input[name=filefoto]").val();
    var kategori = $("#kategori option:selected").val();
    var judul = $("input[name=judul]").val();
    var pengarang = $("input[name=pengarang]").val();
    var penerbit = $("input[name=penerbit]").val();
    var isbn = $("input[name=isbn]").val();
    var tahun_terbit = $("input[name=tahunterbit]").val();
    $.ajax({
        url: 'services.php',
        type: 'POST',
        //masukan parameter
        data: {
            action: action,
            cover: cover,
            id_buku: id_buku,
            id_kategori: kategori,
            judul: judul,
            pengarang: pengarang,
            penerbit: penerbit,
            isbn: isbn,
            tahun_terbit: tahun_terbit
        },
        success: function (data) {
            //menampilkan data di console log
            console.log(data);
            //memasukan data ke html
            alert('Data berhasil di ubah');
            window.location = "index.php";
        },
        error: function (data) {
            console.log(data);
            alert('Data gagal di ubah');
            window.location = "ubah_buku.php";
        }
    })
}

function cari() {
    var action = 'cari_buku';
    var judul = $('#search').val();
    var pengarang = $('#search').val();
    var no = 0;
    if (judul != "" || pengarang != "") {
        $.ajax({
            url: 'services.php',
            type: 'GET',
            //masukan parameter
            data: {
                action: action,
                judul: judul,
                pengarang: pengarang
            },
            success: function (data) {
                //menampilkan data di console log
                console.log(data);
                //memasukan data ke html
                $('#isi-tabel').empty();
                $('#isi-tabel').html("Data tidak ditemukan");
                for (x in data.data) {
                    ++no;
                    $('#isi-tabel').append('<tr id="' + data.data[x].id_buku + '"><td>' + no + '</td>' + '<td><div style="width: 180px"><div class="thumbnail"><img class="img img-responsive" src="img/' + data.data[x].cover + '"></div></div></td>' +
                        '<td data-target="judul">' + data.data[x].judul + '</td>' +
                        '<td>' + data.data[x].pengarang + '</td>' +
                        '<td>' + data.data[x].penerbit + '</td>' +
                        '<td>' + data.data[x].nama_kategori + '</td>' +
                        '<td><a class="btn btn-success" href="ubah_buku.php?id_buku=' + data.data[x].id_buku + '"><i class="fa fa-edit"></i></a>' +
                        '<br><button onclick="delete_data(' + data.data[x].id_buku + ')" class="btn btn-danger"><i class="fa fa-trash"></i></button></td>' +
                        '</tr>');
                }
                $("#search").val("");
            },
            error: function (data) {
                console.log(data);
            }
        })
    } else {
        window.location = "index.php";
    }
}