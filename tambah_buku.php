<html>
<head>
    <title>Rekweb API - tambah buku</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/material-dashboard.css">
    <link rel="stylesheet" href="css/mystyle.css">
    <!-- Fonts and icons -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons' rel='stylesheet'
          type='text/css'>
</head>
<body>
<div class="main-panel">
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header" data-background-color="green">
                            <h4 class="title">Tambah Data Buku</h4>
                        </div>
                        <div class="card-content">
                            <form action="" method="post" enctype="multipart/form-data" id="form-buku">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group label-floating">
                                            <label class="control-label">Kategori</label>
                                            <select class="form-control" name="kategori" id="kategori">
                                                <option value="#" disabled selected></option>
                                                <option value="1" selected>Fiksi</option>
                                                <option value="2" selected>Materi</option>
                                                <option value="3" selected>Komedi</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group label-floating">
                                            <label class="control-label">Judul Buku</label>
                                            <input type="text" class="form-control" name="judul">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group label-floating">
                                            <label class="control-label">Nama Pengarang</label>
                                            <input type="text" class="form-control" name="pengarang">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group label-floating">
                                            <label class="control-label">Penerbit</label>
                                            <input type="text" class="form-control" name="penerbit">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group label-floating">
                                            <label class="control-label">Tahun Terbit</label>
                                            <input type="text" class="form-control" name="tahunterbit">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group label-floating">
                                            <label class="control-label">ISBN</label>
                                            <input type="number" min="0" class="form-control" name="isbn">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group label-floating">
                                            <label class="control-label">Nama Gambar</label>
                                            <input type="text" class="form-control" name="filefoto">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group label-floating">
                                            <label class="control-label">Sinopsis Buku</label>
                                            <textarea class="form-control" rows="5" name="sinopsis"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-success pull-right" style="margin: 0 20px
											20px 0;"><i class="fa fa-save"></i> Simpan
                                </button>
                                <div class="clearfix"></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="footer">
                <div class="container-fluid">
                    <p class="copyright pull-right">
                        &copy;
                        <script>document.write(new Date().getFullYear())</script>
                        Teknik Informatika
                        Universitas Pasundan.
                    </p>
                </div>
            </footer>
        </div>
    </div>
</div>
<script src="js/jquery-3.1.0.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/myscript.js"></script>
<script>
    $(document).ready(function () {
        /* attach a submit handler to the form */
        $("#form-buku").submit(function (event) {
            /* stop form from submitting normally */
            event.preventDefault();
            tambah_data();
        });
    })
</script>
</body>
</html>