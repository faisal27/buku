<html>
<head><title>Rekweb API</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/material-dashboard.css">
    <link rel="stylesheet" href="css/mystyle.css"> <!-- Fonts and icons -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons' rel='stylesheet'
          type='text/css'>
</head>
<body>
<div class="main-panel">
    <nav class="navbar navbar-transparent navbar-absolute">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse"><span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">BooKooKoo</a></div>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#pablo" class="dropdown-toggle" data-toggle="dropdown"> <i class="material-icons">person</i>
                            <p class="hidden-lg hidden-md">Admin</p></a></li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <a href="tambah_buku.php" class="btn btn-success pull-left">Tambah Buku</a>
                    <form action="" method="get">
                        <div class="col-md-2" style="margin-top: -10px;">
                            <div class="form-group label-floating">
                                <label class="control-label">Search</label>
                                <input type="text" class="form-control" name="search" id="search">
                                <button type="button" id="button" class="btn btn-primary"
                                        style="position: absolute; top: -20px; left: 200px">Search
                                </button>
                            </div>
                        </div>
                    </form>
                    <div class="card">
                        <div class="card-header" data-background-color="green"><h4 class="title"><i
                                        class="fa fa-book pull-left"></i> Daftar Buku </h4></div>
                        <div class="card-content table-responsive">
                            <table class="table table-striped table-hover">
                                <thead class="text-success">
                                <td>No.</td>
                                <td>Cover</td>
                                <td>Judul Buku</td>
                                <td>Pengarang</td>
                                <td>Penerbit</td>
                                <td>Kategori</td>
                                <td>Opsi</td>
                                </thead>
                                <tbody id="isi-tabel"></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <footer class="footer">
        <div class="container-fluid">
            <p class="copyright pull-right"> &copy;
                <script>document.write(new Date().getFullYear())</script>
                Teknik Informatika Universitas Pasundan.
            </p>
        </div>
    </footer>
</div>
<script src="js/myscript.js"></script>
<script src="js/jquery-3.1.0.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
    $(document).ready(function () {
        get_buku();
    })
</script>
<script>
    $(document).ready(function () {
        $("#button").click(function (e) {
            e.preventDefault();
            cari();
        });
    })
</script>
</body>
</html>
