<?php

require 'koneksi.php';
header('content-type: application/json; charset=utf-8');
$action = $_REQUEST["action"];
if ($action == "get_data") {
    get_data();
} else if ($action == "insert_buku") {
    insert_buku();
} else if ($action == "delete_buku") {
    delete_buku();
} else if ($action == "update_buku") {
    update_buku();
} else if ($action == "cari_buku") {
    cari_buku();
} else if ($action == "get_all"){
    get_all();
}

function get_all()
{
    $conn = koneksi();
    $sql = "SELECT * FROM buku b, kategori k WHERE b.id_kategori = k.id_kategori";
    //echo $sql."<br />" ;
    $result = mysqli_query($conn, $sql);
    $rows = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $rows[] = $row;
    }
    $response["success"] = "1";
    $response["message"] = "Success";
    $response["data"] = $rows;
    echo json_encode($response);
}

function get_data()
{
    $conn = koneksi();
    $sql = "SELECT * FROM buku b, kategori k WHERE b.id_kategori = k.id_kategori AND id_buku ='".$_GET['id_buku']."'";
    $result = mysqli_query($conn, $sql);
    $rows = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $rows[] = $row;
    }
    $response["success"] = "1";
    $response["message"] = "Success";
    $response["data"] = $rows;
    echo json_encode($response);
}

function insert_buku()
{
    $conn = Koneksi();
    $sql = "insert into buku values (null, '" . $_REQUEST['cover'] . "',
	'" . $_REQUEST['judul'] . "', '" . $_REQUEST['pengarang'] . "', '" . $_REQUEST['penerbit'] . "',
	'" . $_REQUEST['id_kategori'] . "', '" . $_REQUEST['isbn'] . "',
	'" . $_REQUEST['tahun_terbit'] . "')";
    $result = mysqli_query($conn, $sql);
    if ($result) {
        $response["success"] = "1";
        $response["message"] = "Data Berhasil Disimpan";
        echo json_encode($response);
    } else {
        $response["success"] = "0";
        $response["message"] = "Data Gagal Disimpan";
        echo json_encode($response);
    }
}

function delete_buku()
{
    $conn = Koneksi();
    $id = $_GET["id_buku"];
    $sql = "delete from buku where id_buku = $id";
    $result = mysqli_query($conn, $sql);
    if ($result) {
        $response["success"] = "1";
        $response["message"] = "Data Berhasil Dihapus";
        echo json_encode($response);
    } else {
        $response["success"] = "0";
        $response["message"] = "Data Gagal Dihapus";
        echo json_encode($response);
    }
}

function update_buku()
{
    $conn = Koneksi();
//    $id = $_POST["id_buku"];
    $sql = "UPDATE buku SET 
            cover = '" . $_REQUEST['cover'] . "', 
            judul = '" . $_REQUEST['judul'] . "', 
            pengarang = '" . $_REQUEST['pengarang'] . "', 
            penerbit = '" . $_REQUEST['penerbit'] . "', 
            id_kategori = '" . $_REQUEST['id_kategori'] . "', 
            isbn = '" . $_REQUEST['isbn'] . "', 
            tahun_terbit = '" . $_REQUEST['tahun_terbit'] . "' 
            WHERE id_buku = '" . $_REQUEST['id_buku'] . "'";

    $result = mysqli_query($conn, $sql);
    if ($result) {
        $response["success"] = "1";
        $response["message"] = "Data Berhasil Diubah";
        echo json_encode($response);
    } else {
        $response["success"] = "0";
        $response["message"] = "Data Gagal Diubah";
        echo json_encode($response);
    }
}

function cari_buku()
{
    $conn = Koneksi();
    $sql = "SELECT * FROM buku b, kategori k WHERE b.id_kategori = k.id_kategori AND
b.judul = '" . $_GET['judul'] . "' or
b.pengarang = '" . $_GET['pengarang'] . "' ";
    $result = mysqli_query($conn, $sql);
    $rows = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $rows[] = $row;
    }
    $response["success"] = "1";
    $response["message"] = "Success";
    $response["data"] = $rows;
    echo json_encode($response);
}

?>